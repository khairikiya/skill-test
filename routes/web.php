<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\PimproController;
use App\Http\Controllers\auth\AuthController;
use App\Http\Controllers\OperatorController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AuthController::class, 'index'])->name('login');
Route::post('login', [AuthController::class, 'checkLogin'])->name('login.submit');
Route::get('logout', [AuthController::class, 'logout'])->name('logout');

Route::group(['middleware' => ['auth', 'is_login']], function () {

    Route::group(['prefix' => 'admin', 'is_login' => 'Admin'], function () {
        //manage user
        Route::get('dashboard', [AdminController::class, 'admin_dashboard'])->name('admin.dashboard');
        Route::get('manage_user', [AdminController::class, 'manage_user'])->name('admin.manage_user');
        Route::get('getUserData', [AdminController::class, 'getUserData'])->name('admin.getUserData');
        Route::get('getUserRoles', [AdminController::class, 'getUserRoles'])->name('admin.getUserRoles');
        Route::post('updateUserData', [AdminController::class, 'updateUserData'])->name('admin.updateUserData');
        Route::post('addUserData', [AdminController::class, 'addUserData'])->name('admin.addUserData');

        //manage kelurahan
        Route::get('manage_kelurahan', [AdminController::class, 'manage_kelurahan'])->name('admin.manage_kelurahan');
        Route::post('addKelurahanData', [AdminController::class, 'addKelurahanData'])->name('admin.addKelurahanData');
        Route::get('getKelurahanData', [AdminController::class, 'getKelurahanData'])->name('admin.getKelurahanData');
        Route::post('updateKelurahanData', [AdminController::class, 'updateKelurahanData'])->name('admin.updateKelurahanData');
    });

    Route::group(['prefix' => 'operator', 'is_login' => 'Operator'], function () {
        Route::get('dashboard', [OperatorController::class, 'operator_dashboard'])->name('operator.dashboard');
        Route::get('manage_pasien', [OperatorController::class, 'manage_pasien'])->name('operator.manage_pasien');
        Route::get('getKelurahan', [OperatorController::class, 'getKelurahan'])->name('operator.getKelurahan');
        Route::post('addPasienData', [OperatorController::class, 'addPasienData'])->name('operator.addPasienData');
        Route::get('getPasienData', [OperatorController::class, 'getPasienData'])->name('operator.getPasienData');
        Route::get('printPasienData/{id}', [OperatorController::class, 'printPasienData'])->name('operator.printPasienData');
    });
});
