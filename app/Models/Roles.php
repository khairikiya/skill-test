<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    use HasFactory;
    protected $primaryKey = 'role_id';
    protected $table = 't_ref_role';

    protected $fillable = [
        'role_id', 'role_name'
    ];
}
