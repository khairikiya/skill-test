<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pasien extends Model
{
    use HasFactory;
    protected $table = 't_trans_pasien';
    public $timestamps = false;

    protected $fillable = [
        'pasien_id',
        'nama_pasien',
        'alamat',
        'no_telepon',
        'rt_rw',
        'kelurahan_id',
        'tanggal_lahir',
        'jenis_kelamin'
    ];

    public function kelurahan()
    {
        return $this->belongsTo('App\Models\Kelurahan', 'kelurahan_id', 'kelurahan_id');
    }
}
