<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
    use HasFactory;
    protected $primaryKey = 'kelurahan_id';
    protected $table = 't_mst_kelurahan';
    public $timestamps = false;

    protected $fillable = [
        'kelurahan_id',
        'nama_kelurahan',
        'nama_kecamatan',
        'nama_kota',
        'user_id'
    ];
}
