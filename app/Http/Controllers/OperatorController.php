<?php

namespace App\Http\Controllers;

use App\Models\Kelurahan;
use App\Models\Pasien;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Roles;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use PDF;
use DataTables;

class OperatorController extends Controller
{

    public function operator_dashboard()
    {
        $page['name'] = 'dashboard';
        return view('operator.operator_dashboard', $page);
    }
    public function manage_pasien()
    {
        $page['name'] = 'manage_pasien';
        return view('operator.manage_pasien', $page);
    }
    public function getKelurahan()
    {

        $data = Kelurahan::where('STATUS', '1')->get();
        return response()->json($data, Response::HTTP_OK);
    }

    public function addPasienData(Request $request)
    {
        // validasi data ketika user ingin mendaftar akun baru
        $validator = Validator::make($request->all(), [
            'nama_pasien' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
            'tanggal_lahir' => 'required',
            'no_telepon' => 'required'
        ]);


        if ($validator->fails()) {
            return response()->json(['message' => 'This field should not be left blank', 'error' => $validator], Response::HTTP_FORBIDDEN);
        }

        $pasien = Pasien::all();
        $last_seq = count($pasien) + 1;
        $pasienData = new Pasien();
        $pasienData->pasien_id = DATE('ym') . str_pad($last_seq, 6, "0", STR_PAD_LEFT);
        $pasienData->nama_pasien = $request->nama_pasien;
        $pasienData->alamat = $request->alamat;
        $pasienData->kelurahan_id = $request->kelurahan_id;
        $pasienData->tanggal_lahir = $request->tanggal_lahir;
        $pasienData->no_telepon = $request->no_telepon;
        $pasienData->jenis_kelamin = $request->jenis_kelamin;
        $pasienData->rt_rw = $request->rt_rw;
        $pasienData->user_id = Auth::User()->user_id;
        $pasienData->status = 1;

        $pasienData->save();

        return response()->json(['message' => 'Add pasien success', Response::HTTP_OK]);
    }

    public function getPasienData()
    {
        $pasien = Pasien::with([
            'kelurahan'
        ]);


        $data = [];
        foreach ($pasien->get() as $item) {
            array_push($data, [
                'data'         => $item,
            ]);
        }
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn = '<a href="' .  route('operator.printPasienData', $row['data']->pasien_id) . '"                       
                        ><i class="bx bx-printer"></i></a>';
                return $btn;
            })
            ->rawColumns(['action', 'status'])
            ->make(true);
    }

    public function printPasienData($pasien_id)
    {
        $pasien = Pasien::with([
            'kelurahan'
        ])->where('pasien_id', $pasien_id)->first();

        // $pasien = Pasien::where('pasien_id', $pasien_id)->first();

        $data = [
            'pasien_id' => $pasien->pasien_id,
            'nama_pasien' => $pasien->nama_pasien,
            'alamat' => $pasien->alamat,
            'tanggal_lahir' => $pasien->tanggal_lahir,
            'no_telepon' => $pasien->no_telepon,
            'jenis_kelamin' => $pasien->jenis_kelamin,
            'rt_rw' => $pasien->rt_rw,
            'nama_kelurahan' => $pasien->kelurahan->nama_kelurahan

        ];

        $pdf = PDF::loadView('operator.pasienPDF', $data);

        return $pdf->download('DataPasien' . date('Y-M-d-h-i-s') . '.pdf');
    }
}
