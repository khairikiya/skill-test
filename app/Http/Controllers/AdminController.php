<?php

namespace App\Http\Controllers;

use App\Models\Kelurahan;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Roles;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use DataTables;


class AdminController extends Controller
{


    /*
        Admin dashboard

    */

    public function admin_dashboard()
    {


        return view('administrator.admin_dashboard');
    }
    /*
        Manage User
    */

    public function manage_user()
    {
        $page['name'] = 'manage_user';
        return view('administrator.manage_user', $page);
    }

    public function manage_kelurahan()
    {
        $page['name'] = 'manage_kelurahan';
        return view('administrator.manage_kelurahan', $page);
    }

    public function getUserData()
    {
        $user = User::with([
            'roles'
        ]);
        $data = [];
        foreach ($user->get() as $item) {
            array_push($data, [
                'data'         => $item,
            ]);
        }
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('status', function ($row) {
                $user_status = '';
                if ($row['data']->status == '1') {
                    $user_status = "<span class='badge bg-label-primary me-1'>Active</span>";
                } else {
                    $user_status = "<span class='badge bg-label-danger me-1'>Inactive</span>";
                }
                return $user_status;
            })
            ->addColumn('action', function ($row) {
                $btn = '<button href="javascript:void(0)" class="btn btn-primary btn-sm btn-edit mx-2" 
                        data-user_id="' . $row['data']->user_id . '"
                        data-name="' . $row['data']->name . '"
                        data-phone_number="' . $row['data']->phone_number . '"
                        data-address="' . $row['data']->address . '"
                        data-email="' . $row['data']->email . '"
                        data-role_id="' . $row['data']->role_id . '"
                        data-status="' . $row['data']->status . '"
                        ><i class="bx bx-edit"></i></button>';
                return $btn;
            })
            ->rawColumns(['action', 'status'])
            ->make(true);
    }

    public function getUserRoles()
    {

        $data = Roles::where('STATUS', '1')->get();
        return response()->json($data, Response::HTTP_OK);
    }

    public function updateUserData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone_number' => 'required',
            'address' => 'required',
        ]);


        if ($validator->fails()) {
            return response()->json(['message' => 'This field should not be left blak', 'error' => $validator], Response::HTTP_FORBIDDEN);
        }

        User::where('user_id', $request->user_id)->update(
            [
                "name" => $request->name,
                "address" => $request->address,
                "phone_number" => $request->phone_number,
                "status" => $request->status,
                "role_id" => $request->role_id
            ]
        );

        return response()->json(['message' => 'Successfully updating data'], Response::HTTP_OK);
    }

    public function addUserData(Request $request)
    {
        // validasi data ketika user ingin mendaftar akun baru
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'username' => 'required',
            'address' => 'required',
            'phone_number' => 'required',
            'password' => 'required',
            'name' => 'required',
            'kpassword' => 'required'
        ]);


        if ($validator->fails()) {
            return response()->json(['message' => 'This field should not be left blak', 'error' => $validator], Response::HTTP_FORBIDDEN);
        } else if ($request->kpassword != $request->password) {
            return response()->json(["message" => "Password & Password Confirmation not Valid"], Response::HTTP_NOT_ACCEPTABLE);
        }

        //check apakah email bisa digunakan atau tidak

        $checkEmail = User::where('email', $request->email)->first();
        $checkUsername = User::where('username', $request->username)->first();
        if ($checkEmail) {
            return response()->json(['message' => 'Email already used'], Response::HTTP_NOT_ACCEPTABLE);
        } else if ($checkUsername) {
            return response()->json(['message' => 'Username already used'], Response::HTTP_NOT_ACCEPTABLE);
        } else {
            $userData = new User();
            $userData->name = $request->name;
            $userData->email = $request->email;
            $userData->username = $request->username;
            $userData->address = $request->address;
            $userData->phone_number = $request->phone_number;
            $userData->password = bcrypt($request->password);
            $userData->role_id = $request->role_id;
            $userData->status = $request->status;

            $userData->save();

            return response()->json(['message' => 'Registration Success', Response::HTTP_OK]);
        }
    }


    public function getKelurahanData()
    {
        $kelurahan = Kelurahan::get();

        $data = [];
        foreach ($kelurahan as $item) {
            array_push($data, [
                'data'         => $item,
            ]);
        }
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('status', function ($row) {
                $kelurahan_status = '';
                if ($row['data']->status == '1') {
                    $kelurahan_status = "<span class='badge bg-label-primary me-1'>Active</span>";
                } else {
                    $kelurahan_status = "<span class='badge bg-label-danger me-1'>Inactive</span>";
                }
                return $kelurahan_status;
            })
            ->addColumn('action', function ($row) {
                $btn = '<button href="javascript:void(0)" class="btn btn-primary btn-sm btn-edit mx-2" 
                        data-kelurahan_id="' . $row['data']->kelurahan_id . '"
                        data-nama_kelurahan="' . $row['data']->nama_kelurahan . '"
                        data-nama_kecamatan="' . $row['data']->nama_kecamatan . '"
                        data-nama_kota="' . $row['data']->nama_kota . '"
                        data-status="' . $row['data']->status . '"
                        ><i class="bx bx-edit"></i></button>';
                return $btn;
            })
            ->rawColumns(['action', 'status'])
            ->make(true);
    }
    public function addKelurahanData(Request $request)
    {
        // validasi data ketika user ingin mendaftar akun baru
        $validator = Validator::make($request->all(), [
            'nama_kelurahan' => 'required',
            'nama_kecamatan' => 'required',
            'nama_kota' => 'required'
        ]);


        if ($validator->fails()) {
            return response()->json(['message' => 'This field should not be left blak', 'error' => $validator], Response::HTTP_FORBIDDEN);
        } else if ($request->kpassword != $request->password) {
            return response()->json(["message" => "Password & Password Confirmation not Valid"], Response::HTTP_NOT_ACCEPTABLE);
        }

        $checkKelurahan = Kelurahan::where('nama_kelurahan', $request->nama_kelurahan)->first();
        if ($checkKelurahan) {
            return response()->json(['message' => 'Kelurahan already added'], Response::HTTP_NOT_ACCEPTABLE);
        } else {
            $kelurahanData = new Kelurahan();
            $kelurahanData->nama_kelurahan = $request->nama_kelurahan;
            $kelurahanData->nama_kecamatan = $request->nama_kecamatan;
            $kelurahanData->nama_kota = $request->nama_kota;
            $kelurahanData->status = $request->status;
            $kelurahanData->user_id = Auth::User()->user_id;

            $kelurahanData->save();

            return response()->json(['message' => 'Add kelurahan success', Response::HTTP_OK]);
        }
    }

    public function updateKelurahanData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_kelurahan' => 'required',
            'nama_kota' => 'required',
            'nama_kecamatan' => 'required',
        ]);


        if ($validator->fails()) {
            return response()->json(['message' => 'This field should not be left blak', 'error' => $validator], Response::HTTP_FORBIDDEN);
        }

        Kelurahan::where('kelurahan_id', $request->kelurahan_id)->update(
            [
                "nama_kelurahan" => $request->nama_kelurahan,
                "nama_kota" => $request->nama_kota,
                "nama_kecamatan" => $request->nama_kecamatan,
                "status" => $request->status
            ]
        );

        return response()->json(['message' => 'Successfully updating data'], Response::HTTP_OK);
    }
}
