<?php

namespace App\Http\Controllers\auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;


use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    //check if authenticated, redirect to page base on role
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index()
    {
        return view('auth.login');
    }

    public function register()
    {
        return view('auth.registerPage');
    }

    public function checkLogin(Request $request)
    {
        $input = $request->all();

        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        if (Auth::attempt(array('username' => $input['username'], 'password' => $input['password']))) {
            $url = '';
            if (Auth()->user()->role_id == 1) {
                $url = route('admin.manage_user');
            } else if (Auth()->user()->role_id == 2) {
                $url = route('operator.manage_pasien');
            }
            return response()->json([
                'title' => 'Berhasil Login',
                'message' => 'Mohon ditunggu..',
                'url' => $url
            ], Response::HTTP_OK);
        } else {
            return response()->json([
                'title' => 'Gagal Login',
                'message' => 'Periksa username dan password kembali',
            ], Response::HTTP_UNAUTHORIZED);
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }

    public function registerProcess(Request $request)
    {

        // validasi data ketika user ingin mendaftar akun baru
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
            'name' => 'required',
            'kpassword' => 'required'
        ]);


        if ($validator->fails()) {
            return response()->json(['message' => 'This field should not be left blak', 'error' => $validator], Response::HTTP_FORBIDDEN);
        } else if ($request->kpassword != $request->password) {
            return response()->json(["message" => "Password & Password Confirmation not Valid"], Response::HTTP_FORBIDDEN);
        }

        //check apakah email bisa digunakan atau tidak

        $checkEmail = User::where('email', $request->email)->first();
        if (!$checkEmail) {
            $userData = new User();
            $userData->name = $request->name;
            $userData->email = $request->email;
            $userData->password = bcrypt($request->password);
            $userData->role_id = 2;
            $userData->status = 1;
            $userData->created_at = new \DateTime();

            $userData->save();

            return response()->json(['message' => 'Register Success', Response::HTTP_OK]);
        } else {
            return response()->json(['message' => 'Email telah digunakan'], Response::HTTP_CONFLICT);
        }
    }
}
