<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTTransPasienTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('t_trans_pasien');
        Schema::create('t_trans_pasien', function (Blueprint $table) {
            $table->string('pasien_id');
            $table->string('nama_pasien');
            $table->string('alamat');
            $table->string('no_telepon');
            $table->string('rt_rw');
            $table->date('tanggal_lahir');
            $table->string('jenis_kelamin');
            $table->unsignedBigInteger('kelurahan_id');
            $table->foreign('kelurahan_id')->references('kelurahan_id')->on('t_mst_kelurahan');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('user_id')->on('t_mst_user');
            $table->integer('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_trans_pasien');
    }
}
