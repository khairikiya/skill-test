<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_mst_user', function (Blueprint $table) {
            $table->id('user_id');
            $table->string('name');
            $table->string('email');
            $table->string('username');
            $table->string('address')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('password');
            $table->unsignedBigInteger('role_id');
            $table->foreign('role_id')->references('role_id')->on('t_ref_role');
            $table->integer('status');
        });
        DB::table('t_mst_user')->insert([
            ['name' => 'admin', 'email' => 'admin@gmail.com', 'username' => 'admin', 'role_id' => 1, 'password' => bcrypt('admin'),  'status' => 1],
            ['name' => 'operator', 'email' => 'operator@gmail.com', 'username' => 'operator', 'role_id' => 2, 'password' => bcrypt('operator'),  'status' => 1],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
