<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_ref_role', function (Blueprint $table) {
            $table->id('role_id');
            $table->string('role_name');
            $table->integer('status');
        });

        DB::table('t_ref_role')->insert([
            ['role_name' => 'Admin', 'status' => 1],
            ['role_name' => 'Operator', 'status' => 1]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
