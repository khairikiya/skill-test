<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTMstKelurahanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_mst_kelurahan', function (Blueprint $table) {
            $table->id('kelurahan_id');
            $table->string('nama_kelurahan');
            $table->string('nama_kota');
            $table->string('nama_kecamatan');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('user_id')->on('t_mst_user');
            $table->integer('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_mst_kelurahan');
    }
}
