/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 100420
 Source Host           : localhost:3306
 Source Schema         : skill_test

 Target Server Type    : MySQL
 Target Server Version : 100420
 File Encoding         : 65001

 Date: 30/07/2022 07:15:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_07_28_042128_create_roles_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_12_14_000001_create_personal_access_tokens_table', 1);
INSERT INTO `migrations` VALUES (4, '2022_07_28_045001_create_t_mst_kelurahan_table', 1);
INSERT INTO `migrations` VALUES (5, '2022_07_28_055354_create_t_trans_pasien_table', 1);

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `personal_access_tokens_token_unique`(`token`) USING BTREE,
  INDEX `personal_access_tokens_tokenable_type_tokenable_id_index`(`tokenable_type`, `tokenable_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of personal_access_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for t_mst_kelurahan
-- ----------------------------
DROP TABLE IF EXISTS `t_mst_kelurahan`;
CREATE TABLE `t_mst_kelurahan`  (
  `kelurahan_id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama_kelurahan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_kota` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_kecamatan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `status` int NOT NULL,
  PRIMARY KEY (`kelurahan_id`) USING BTREE,
  INDEX `t_mst_kelurahan_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `t_mst_kelurahan_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `t_mst_user` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_mst_kelurahan
-- ----------------------------
INSERT INTO `t_mst_kelurahan` VALUES (1, 'Cengkareng Timur', 'Jakarta Barat', 'Cengkareng', 1, 1);
INSERT INTO `t_mst_kelurahan` VALUES (2, 'Jelambar', 'Jakarta Barat', 'Grogol', 1, 1);
INSERT INTO `t_mst_kelurahan` VALUES (3, 'Rawasar', 'Jakarta Pusat', 'Cempaka Putih', 1, 1);
INSERT INTO `t_mst_kelurahan` VALUES (4, 'Kebon kelapa', 'Jakarta Pusat', 'Gambir', 1, 1);

-- ----------------------------
-- Table structure for t_mst_user
-- ----------------------------
DROP TABLE IF EXISTS `t_mst_user`;
CREATE TABLE `t_mst_user`  (
  `user_id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `phone_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` bigint UNSIGNED NOT NULL,
  `status` int NOT NULL,
  PRIMARY KEY (`user_id`) USING BTREE,
  INDEX `t_mst_user_role_id_foreign`(`role_id`) USING BTREE,
  CONSTRAINT `t_mst_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `t_ref_role` (`role_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_mst_user
-- ----------------------------
INSERT INTO `t_mst_user` VALUES (1, 'admin', 'admin@gmail.com', 'admin', NULL, NULL, '$2y$10$kt1IhV/yTrtMmDuBdIJz7OAOcwrdYyXiZcN1EaGj2yMYP0FwGqPQW', 1, 1);
INSERT INTO `t_mst_user` VALUES (2, 'operator', 'operator@gmail.com', 'operator', NULL, NULL, '$2y$10$9YGye9nHAGDkywQpE478ROPme78lIjNyvt7ApIWG.Pt3m2yw5ZPtW', 2, 1);
INSERT INTO `t_mst_user` VALUES (3, 'Operator 2', 'operator2@gmail.com', 'operator2', 'Jl. Operator', '0812082082', '$2y$10$ymjxIqPfqlcQvunf5j5.Z.9sW4JSr667gUjGKib1DVQ/er5D27Ce6', 2, 0);

-- ----------------------------
-- Table structure for t_ref_role
-- ----------------------------
DROP TABLE IF EXISTS `t_ref_role`;
CREATE TABLE `t_ref_role`  (
  `role_id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int NOT NULL,
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_ref_role
-- ----------------------------
INSERT INTO `t_ref_role` VALUES (1, 'Admin', 1);
INSERT INTO `t_ref_role` VALUES (2, 'Operator', 1);

-- ----------------------------
-- Table structure for t_trans_pasien
-- ----------------------------
DROP TABLE IF EXISTS `t_trans_pasien`;
CREATE TABLE `t_trans_pasien`  (
  `pasien_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_pasien` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telepon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `rt_rw` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `kelurahan_id` bigint UNSIGNED NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `status` int NOT NULL,
  INDEX `t_trans_pasien_kelurahan_id_foreign`(`kelurahan_id`) USING BTREE,
  INDEX `t_trans_pasien_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `t_trans_pasien_kelurahan_id_foreign` FOREIGN KEY (`kelurahan_id`) REFERENCES `t_mst_kelurahan` (`kelurahan_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `t_trans_pasien_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `t_mst_user` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_trans_pasien
-- ----------------------------
INSERT INTO `t_trans_pasien` VALUES ('2207000001', 'Rio', 'Jl. Rawamangun', '0820982', '01/02', '2022-07-29', 'Laki-laki', 1, 2, 1);
INSERT INTO `t_trans_pasien` VALUES ('2207000002', 'Rina', 'Jl. Taman Sari No.3', '08930830', '01/02', '2022-07-28', 'Perempuan', 3, 2, 1);

SET FOREIGN_KEY_CHECKS = 1;
