@extends('template.app')

@section('title', 'Dashboard | Manage Pasien')

@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
            <div class="col-md 6 col-lg-6 col-sm-6">
                <h4 class="py-3 mb-4"><a href="{{ route('operator.manage_pasien') }}"> Pasien</a>
                </h4>
            </div>
            <div class="col-md 6 col-lg-6 col-sm-6">
                <button id="btn-add" class="btn rounded-pill btn-primary mb-4" style="float: right"><i class="bx bx-plus"></i>
                    Pasien</button>
            </div>

        </div>

        <!-- Responsive Table -->
        <div class="card">

            <h5 class="card-header">Daftar Pasien</h5>
            <div class="table-responsive text-nowrap p-3">
                <table class="table user_datatable">
                    <thead>
                        <tr class="text-nowrap">
                            <th> No</th>
                            <th>ID Pasien</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>No Telepon</th>
                            <th>RT RW</th>
                            <th>Kelurahan</th>
                            <th>Tanggal Lahir</th>
                            <th>Jenis Kelamin</th>
                            <th>Cetak</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
        <!--/ Responsive Table -->
    </div>

    {{-- Modal untuk edit data --}}
    <div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
                    <button type="button" id="close-modal" class="btn btn-sm btn-danger" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Name</label>
                            <input type="text" class="form-control" id="name">
                            <input type="hidden" class="form-control" id="user_id">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Email</label>
                            <input type="text" class="form-control" id="email" disabled>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Phone Number</label>
                            <input type="text" class="form-control" id="phone_number">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Address</label>
                            <input type="text" class="form-control" id="address">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Role</label>
                            <select name="role_id" id="role_id" class="form-control"></select>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Status</label>
                            <br>
                            <div class="form-check form-check-inline mt-3">
                                <input class="form-check-input" type="checkbox" id="status">
                                <label class="form-check-label" for="inlineCheckbox1">Aktif</label>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btn-save" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal add data --}}
    <div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add User</h5>
                    <button type="button" id="close-modal-add" class="btn btn-sm btn-danger" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Nama Pasien</label>
                            <input type="text" class="form-control" id="add_nama_pasien">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Alamat</label>
                            <input type="text" class="form-control" id="add_alamat">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Telepon</label>
                            <input type="text" class="form-control" id="add_no_telepon">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">RT/RW</label>
                            <input type="text" class="form-control" id="add_rt_rw">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Kelurahan</label>
                            <select type="text" class="form-control" id="add_kelurahan_id"></select>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Nama Kecamatan</label>
                            <input type="text" class="form-control" id="nama_kecamatan" disabled>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Nama Kota</label>
                            <input type="text" class="form-control" id="nama_kota" disabled>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Tanggal Lahir</label>
                            <input type="date" class="form-control" id="add_tanggal_lahir">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Jenis Kelamin</label>
                            <select type="password" id="add_jenis_kelamin" class="form-control">
                                <option>Laki-laki</option>
                                <option>Perempuan</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btn-add-save" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('additional-js')
    <script>
        // Config
        $(document).ready(function(e) {
            getDataPasien();
        });

        $(document).on('click', '#close-modal', function(e) {
            $("#modal-edit").modal('toggle');
        })
        $(document).on('click', '#close-modal-add', function(e) {
            $("#modal-add").modal('toggle');
        })
        $(document).on('change', '#add_kelurahan_id', function(e) {

            $("#nama_kecamatan").val($(this).find(':selected').data('nama_kecamatan'))
            $("#nama_kota").val($(this).find(':selected').data('nama_kota'))
        })

        // Action

        $(document).on('click', '#btn-add', function(e) {
            $("#modal-add").modal('show');
            //get data roles
            $.ajax({
                type: 'GET',
                url: `{{ route('operator.getKelurahan') }}`,
                success: function(res) {
                    $("#add_kelurahan_id").empty();
                    $("#add_kelurahan_id").append(`<option>Pilih kelurahan</option>`);
                    $.each(res, function(k, v) {
                        $("#add_kelurahan_id").append(
                            `<option data-nama_kecamatan="${v.nama_kecamatan}" data-nama_kota="${v.nama_kota}" value="${v.kelurahan_id}">${v.nama_kelurahan}</option>`
                        )
                    })
                },
                error: function(jqXHR, error, errorThrown) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Login Failed',
                        text: 'Something when wrong',
                        showConfirmButton: false,
                        timer: '2000'
                    })

                }
            })
        })

        $(document).on('click', '#btn-add-save', function(e) {

            $("#modal-add").modal('toggle');

            var nama_pasien = $("#add_nama_pasien").val()
            var alamat_pasien = $("#add_alamat").val()
            var no_telepon = $("#add_no_telepon").val()
            var rt_rw = $("#add_rt_rw").val()
            var kelurahan_id = $("#add_kelurahan_id").val()
            var tanggal_lahir = $("#add_tanggal_lahir").val()
            var jenis_kelamin = $("#add_jenis_kelamin").val()

            $.ajax({
                type: 'POST',
                url: `{{ route('operator.addPasienData') }}`,
                data: {
                    nama_pasien: nama_pasien,
                    alamat: alamat_pasien,
                    no_telepon: no_telepon,
                    rt_rw: rt_rw,
                    kelurahan_id: kelurahan_id,
                    tanggal_lahir: tanggal_lahir,
                    jenis_kelamin: jenis_kelamin,
                    _token: `{{ csrf_token() }}`
                },
                success: function(res) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Success',
                        text: 'Add pasien success',
                        showConfirmButton: true,
                    })
                    getDataPasien();
                },
                error: function(jqXHR, error, errorThrown) {
                    console.log(jqXHR);

                    if (jqXHR.status == 406) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Failed',
                            text: jqXHR.responseJSON.message,
                            showConfirmButton: false,
                            timer: '2000'
                        })
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Failed',
                            text: 'Adding Pasien Fail, Periksa inputan kembali',
                            showConfirmButton: false,
                            timer: '2000'
                        })
                    }
                }
            })

        })

        $(document).on('click', '.btn-edit', function(e) {
            $("#modal-edit").modal('show');
            var data_role = $(this).data('role_id');

            // set data to modal
            $("#user_id").val($(this).data('user_id'))
            $("#name").val($(this).data('name'))
            $("#email").val($(this).data('email'))
            $("#phone_number").val($(this).data('phone_number'))
            $("#address").val($(this).data('address'))
            if ($(this).data('status') == 1) {
                $("#status").prop('checked', true);
                $("#status").val(1)
            }

            //get data roles
            $.ajax({
                type: 'GET',
                url: `{{ route('admin.getUserRoles') }}`,
                success: function(res) {
                    $("#role_id").empty();
                    $.each(res, function(k, v) {
                        $("#role_id").append(
                            `<option data-value="${v.role_id}" value="${v.role_id}">${v.role_name}</option>`
                        )
                    })
                    $("#role_id option[data-value='" + data_role + "']").attr("selected",
                        "selected");

                },
                error: function(jqXHR, error, errorThrown) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Login Failed',
                        text: 'Something when wrong',
                        showConfirmButton: false,
                        timer: '2000'
                    })

                }
            })
        })

        $(document).on('click', '#btn-save', function(e) {

            $("#modal-edit").modal('toggle');

            var name = $("#name").val()
            var user_id = $("#user_id").val()
            var phone_number = $("#phone_number").val()
            var address = $("#address").val()
            var role_id = $("#role_id option:selected").val()
            var status = ''
            if ($('#status').is(':checked')) {
                status = 1
            } else {
                status = 0
            }

            $.ajax({
                type: 'POST',
                url: `{{ route('admin.updateUserData') }}`,
                data: {
                    user_id: user_id,
                    name: name,
                    phone_number: phone_number,
                    role_id: role_id,
                    status: status,
                    address: address,
                    _token: `{{ csrf_token() }}`
                },
                success: function(res) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Success',
                        text: res.message,
                        showConfirmButton: false,
                        timer: '2000'
                    })
                    getDataPasien();
                },
                error: function(jqXHR, error, errorThrown) {

                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: 'This field should not be left blank',
                        showConfirmButton: false,
                        timer: '2000'
                    })

                }
            })

        })

        function getDataPasien() {
            $('.user_datatable').DataTable().destroy();
            var table = $('.user_datatable').DataTable({
                lengthChange: false,
                processing: true,
                responsive: true,
                autoWidth: false,
                oLanguage: {
                    oPaginate: {
                        sNext: '<i>Next</i>',
                        sPrevious: '<i>Previous</i> '
                    }
                },
                iDisplayLength: 5,
                pagingType: 'simple_numbers',
                serverSide: true,
                ajax: `{{ route('operator.getPasienData') }}`,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: true,
                    },
                    {
                        data: 'data.pasien_id',
                        name: 'data.pasien_id',
                        orderable: false,
                    },
                    {
                        data: 'data.nama_pasien',
                        name: 'data.nama_pasien',
                        orderable: false,
                    },
                    {
                        data: 'data.alamat',
                        name: 'data.alamat',
                        orderable: false,
                    },
                    {
                        data: 'data.no_telepon',
                        name: 'data.no_telepon',
                        orderable: false,
                    },
                    {
                        data: 'data.rt_rw',
                        name: 'data.rt_rw',
                        orderable: false,
                    },
                    {
                        data: 'data.kelurahan.nama_kelurahan',
                        name: 'data.kelurahan.nama_kelurahan',
                        orderable: false,
                    },
                    {
                        data: 'data.tanggal_lahir',
                        name: 'data.tanggal_lahir',
                        orderable: false,
                    },
                    {
                        data: 'data.jenis_kelamin',
                        name: 'data.jenis_kelamin',
                        orderable: false,
                    },
                    {
                        data: 'action',
                        name: 'data.action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });
        }
    </script>

@endsection
