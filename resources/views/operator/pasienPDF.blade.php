<!DOCTYPE html>
<html>

<head>
    <style>
        table,
        td,
        th {
            border: 0px;
        }

        table {
            width: 100%;
            border-collapse: collapse;
        }
    </style>
</head>

<body>

    <h2>Pasien {{ $nama_pasien }} - ({{ $pasien_id }})</h2>

    <table>
        <tr>
            <td>ID Pasien</td>
            <td>{{ $pasien_id }}</td>
        </tr>
        <tr>
            <td>Nama Lengkap</td>
            <td>{{ $nama_pasien }}</td>
        </tr>
        <tr>
            <td>Jenis Kelamin</td>
            <td>{{ $jenis_kelamin }}</td>
        </tr>
        <tr>
            <td>Alamat Pasien</td>
            <td>{{ $alamat }}</td>
        </tr>
        <tr>
            <td>RT/RW</td>
            <td>{{ $rt_rw }}</td>
        </tr>
        <tr>
            <td>No Telepon</td>
            <td>{{ $no_telepon }}</td>
        </tr>
        <tr>
            <td>Kelurahan</td>
            <td>{{ $nama_kelurahan }}</td>
        </tr>


    </table>

</body>

</html>
