@extends('template.app')

@section('title', 'Dashboard | Manage Kelurahan')

@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
            <div class="col-md 6 col-lg-6 col-sm-6">
                <h4 class="py-3 mb-4"><span class="text-muted fw-light">Master Data /</span><a
                        href="{{ route('admin.manage_user') }}"> Kelurahan</a>
                </h4>
            </div>
            <div class="col-md 6 col-lg-6 col-sm-6">
                <button id="btn-add" class="btn rounded-pill btn-primary mb-4" style="float: right"><i
                        class="bx bx-plus"></i>
                    Kelurahan</button>
            </div>

        </div>

        <!-- Responsive Table -->
        <div class="card">

            <h5 class="card-header">Daftar Kelurahan</h5>
            <div class="table-responsive text-nowrap p-3">
                <table class="table user_datatable">
                    <thead>
                        <tr class="text-nowrap">
                            <th> No</th>
                            <th>Nama Kelurahan</th>
                            <th>Nama Kecamatan</th>
                            <th>Nama Kota</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
        <!--/ Responsive Table -->
    </div>

    {{-- Modal untuk edit data --}}
    <div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Kelurahan</h5>
                    <button type="button" id="close-modal" class="btn btn-sm btn-danger" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Nama Kelurahan</label>
                            <input type="text" class="form-control" id="nama_kelurahan">
                            <input type="hidden" class="form-control" id="kelurahan_id">
                        </div>

                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Nama Kecamatan</label>
                            <input type="text" class="form-control" id="nama_kecamatan">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Nama Kota</label>
                            <input type="text" class="form-control" id="nama_kota">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Status</label>
                            <br>
                            <div class="form-check form-check-inline mt-3">
                                <input class="form-check-input" type="checkbox" id="status">
                                <label class="form-check-label" for="inlineCheckbox1">Aktif</label>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btn-save" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal add data --}}
    <div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Kelurahan</h5>
                    <button type="button" id="close-modal-add" class="btn btn-sm btn-danger" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Nama Kelurahan</label>
                            <input type="text" class="form-control" id="add_nama_kelurahan">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Nama Kota</label>
                            <input type="text" class="form-control" id="add_nama_kota">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Nama Kecamatan</label>
                            <input type="text" class="form-control" id="add_nama_kecamatan">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Status</label>
                            <br>
                            <div class="form-check form-check-inline mt-3">
                                <input class="form-check-input" type="checkbox" id="add_status">
                                <label class="form-check-label" for="inlineCheckbox1">Aktif</label>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btn-add-save" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('additional-js')
    <script>
        // Config
        $(document).ready(function(e) {
            getDataKelurahan();
        });

        $(document).on('click', '#close-modal', function(e) {
            $("#modal-edit").modal('toggle');
        })
        $(document).on('click', '#close-modal-add', function(e) {
            $("#modal-add").modal('toggle');
        })
        $(document).on('click', '#btn-add', function(e) {
            $("#modal-add").modal('show');
        });

        // Action


        $(document).on('click', '#btn-add-save', function(e) {

            $("#modal-add").modal('toggle');

            var nama_kelurahan = $("#add_nama_kelurahan").val()
            var nama_kota = $("#add_nama_kota").val()
            var nama_kecamatan = $("#add_nama_kecamatan").val()
            var status = ''
            if ($('#add_status').is(':checked')) {
                status = 1
            } else {
                status = 0
            }

            $.ajax({
                type: 'POST',
                url: `{{ route('admin.addKelurahanData') }}`,
                data: {
                    nama_kelurahan: nama_kelurahan,
                    nama_kota: nama_kota,
                    nama_kecamatan: nama_kecamatan,
                    status: status,
                    _token: `{{ csrf_token() }}`
                },
                success: function(res) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Success',
                        text: 'Add Kelurahan success',
                        showConfirmButton: true,
                    })
                    getDataKelurahan();
                },
                error: function(jqXHR, error, errorThrown) {
                    console.log(jqXHR);

                    if (jqXHR.status == 406) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Failed',
                            text: jqXHR.responseJSON.message,
                            showConfirmButton: false,
                            timer: '2000'
                        })
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Failed',
                            text: 'Adding user fail',
                            showConfirmButton: false,
                            timer: '2000'
                        })
                    }
                }
            })

        })

        $(document).on('click', '.btn-edit', function(e) {
            $("#modal-edit").modal('show');

            // set data to modal
            $("#kelurahan_id").val($(this).data('kelurahan_id'))
            $("#nama_kelurahan").val($(this).data('nama_kelurahan'))
            $("#nama_kota").val($(this).data('nama_kota'))
            $("#nama_kecamatan").val($(this).data('nama_kecamatan'))
            if ($(this).data('status') == 1) {
                $("#status").prop('checked', true);
                $("#status").val(1)
            }

        })

        $(document).on('click', '#btn-save', function(e) {

            $("#modal-edit").modal('toggle');

            var nama_kelurahan = $("#nama_kelurahan").val()
            var kelurahan_id = $("#kelurahan_id").val()
            var nama_kota = $("#nama_kota").val()
            var nama_kecamatan = $("#nama_kecamatan").val()
            var status = ''
            if ($('#status').is(':checked')) {
                status = 1
            } else {
                status = 0
            }

            $.ajax({
                type: 'POST',
                url: `{{ route('admin.updateKelurahanData') }}`,
                data: {
                    kelurahan_id: kelurahan_id,
                    nama_kelurahan: nama_kelurahan,
                    nama_kota: nama_kota,
                    nama_kecamatan: nama_kecamatan,
                    status: status,
                    _token: `{{ csrf_token() }}`
                },
                success: function(res) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Success',
                        text: res.message,
                        showConfirmButton: false,
                        timer: '2000'
                    })
                    getDataKelurahan();
                },
                error: function(jqXHR, error, errorThrown) {

                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: 'This field should not be left blank',
                        showConfirmButton: false,
                        timer: '2000'
                    })

                }
            })

        })

        function getDataKelurahan() {
            $('.user_datatable').DataTable().destroy();
            var table = $('.user_datatable').DataTable({
                lengthChange: false,
                processing: true,
                responsive: true,
                autoWidth: false,
                oLanguage: {
                    oPaginate: {
                        sNext: '<i>Next</i>',
                        sPrevious: '<i>Previous</i> '
                    }
                },
                iDisplayLength: 5,
                pagingType: 'simple_numbers',
                serverSide: true,
                ajax: `{{ route('admin.getKelurahanData') }}`,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: true,
                    },
                    {
                        data: 'data.nama_kelurahan',
                        name: 'data.nama_kelurahan',
                        orderable: false,
                    },
                    {
                        data: 'data.nama_kecamatan',
                        name: 'data.nama_kecamatan',
                        orderable: false,
                    },
                    {
                        data: 'data.nama_kota',
                        name: 'data.nama_kota',
                        orderable: false,
                    },
                    {
                        data: 'status',
                        name: 'data.status',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'action',
                        name: 'data.action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });
        }
    </script>

@endsection
