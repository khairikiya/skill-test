@extends('template.app')

@section('title', 'Dashboard | Manage User')

@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
            <div class="col-md 6 col-lg-6 col-sm-6">
                <h4 class="py-3 mb-4"><span class="text-muted fw-light">Master Data /</span><a
                        href="{{ route('admin.manage_user') }}"> Users</a>
                </h4>
            </div>
            <div class="col-md 6 col-lg-6 col-sm-6">
                <button id="btn-add" class="btn rounded-pill btn-primary mb-4" style="float: right"><i
                        class="bx bx-plus"></i>
                    User</button>
            </div>

        </div>

        <!-- Responsive Table -->
        <div class="card">

            <h5 class="card-header">Daftar User</h5>
            <div class="table-responsive text-nowrap p-3">
                <table class="table user_datatable">
                    <thead>
                        <tr class="text-nowrap">
                            <th> No</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Username</th>
                            <th>Role</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
        <!--/ Responsive Table -->
    </div>

    {{-- Modal untuk edit data --}}
    <div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
                    <button type="button" id="close-modal" class="btn btn-sm btn-danger" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Name</label>
                            <input type="text" class="form-control" id="name">
                            <input type="hidden" class="form-control" id="user_id">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Email</label>
                            <input type="text" class="form-control" id="email" disabled>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Phone Number</label>
                            <input type="text" class="form-control" id="phone_number">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Address</label>
                            <input type="text" class="form-control" id="address">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Role</label>
                            <select name="role_id" id="role_id" class="form-control"></select>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Status</label>
                            <br>
                            <div class="form-check form-check-inline mt-3">
                                <input class="form-check-input" type="checkbox" id="status">
                                <label class="form-check-label" for="inlineCheckbox1">Aktif</label>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btn-save" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal add data --}}
    <div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add User</h5>
                    <button type="button" id="close-modal-add" class="btn btn-sm btn-danger" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Name</label>
                            <input type="text" class="form-control" id="add_name">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Email</label>
                            <input type="text" class="form-control" id="add_email">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Username</label>
                            <input type="text" class="form-control" id="add_username">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Phone Number</label>
                            <input type="text" class="form-control" id="add_phone_number">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Address</label>
                            <input type="text" class="form-control" id="add_address">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Role</label>
                            <select name="role_id" id="add_role_id" class="form-control"></select>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Password</label>
                            <input type="password" id="add_password" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Password Confirmation</label>
                            <input type="password" id="kpassword" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Status</label>
                            <br>
                            <div class="form-check form-check-inline mt-3">
                                <input class="form-check-input" type="checkbox" id="add_status">
                                <label class="form-check-label" for="inlineCheckbox1">Aktif</label>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btn-add-save" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('additional-js')
    <script>
        // Config
        $(document).ready(function(e) {
            getDataUser();
        });

        $(document).on('click', '#close-modal', function(e) {
            $("#modal-edit").modal('toggle');
        })
        $(document).on('click', '#close-modal-add', function(e) {
            $("#modal-add").modal('toggle');
        })

        // Action

        $(document).on('click', '#btn-add', function(e) {
            $("#modal-add").modal('show');
            //get data roles
            $.ajax({
                type: 'GET',
                url: `{{ route('admin.getUserRoles') }}`,
                success: function(res) {
                    $("#role_id").empty();
                    $.each(res, function(k, v) {
                        $("#add_role_id").append(
                            `<option data-value="${v.role_id}" value="${v.role_id}">${v.role_name}</option>`
                        )
                    })
                },
                error: function(jqXHR, error, errorThrown) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Login Failed',
                        text: 'Something when wrong',
                        showConfirmButton: false,
                        timer: '2000'
                    })

                }
            })
        })

        $(document).on('click', '#btn-add-save', function(e) {

            $("#modal-add").modal('toggle');

            var name = $("#add_name").val()
            var email = $("#add_email").val()
            var user_id = $("#add_user_id").val()
            var username = $("#add_username").val()
            var phone_number = $("#add_phone_number").val()
            var address = $("#add_address").val()
            var password = $("#add_password").val()
            var kpassword = $("#kpassword").val()
            var role_id = $("#add_role_id option:selected").val()
            var status = ''
            if ($('#add_status').is(':checked')) {
                status = 1
            } else {
                status = 0
            }

            $.ajax({
                type: 'POST',
                url: `{{ route('admin.addUserData') }}`,
                data: {
                    email: email,
                    name: name,
                    username: username,
                    phone_number: phone_number,
                    role_id: role_id,
                    status: status,
                    address: address,
                    password: password,
                    kpassword: kpassword,
                    _token: `{{ csrf_token() }}`
                },
                success: function(res) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Success',
                        text: res.message + ' Username: ' + username + ', Password : ' +
                            password,
                        showConfirmButton: true,
                    })
                    getDataUser();
                },
                error: function(jqXHR, error, errorThrown) {
                    console.log(jqXHR);

                    if (jqXHR.status == 406) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Failed',
                            text: jqXHR.responseJSON.message,
                            showConfirmButton: false,
                            timer: '2000'
                        })
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Failed',
                            text: 'Adding user fail',
                            showConfirmButton: false,
                            timer: '2000'
                        })
                    }
                }
            })

        })

        $(document).on('click', '.btn-edit', function(e) {
            $("#modal-edit").modal('show');
            var data_role = $(this).data('role_id');

            // set data to modal
            $("#user_id").val($(this).data('user_id'))
            $("#name").val($(this).data('name'))
            $("#email").val($(this).data('email'))
            $("#phone_number").val($(this).data('phone_number'))
            $("#address").val($(this).data('address'))
            if ($(this).data('status') == 1) {
                $("#status").prop('checked', true);
                $("#status").val(1)
            }

            //get data roles
            $.ajax({
                type: 'GET',
                url: `{{ route('admin.getUserRoles') }}`,
                success: function(res) {
                    $("#role_id").empty();
                    $.each(res, function(k, v) {
                        $("#role_id").append(
                            `<option data-value="${v.role_id}" value="${v.role_id}">${v.role_name}</option>`
                        )
                    })
                    $("#role_id option[data-value='" + data_role + "']").attr("selected",
                        "selected");

                },
                error: function(jqXHR, error, errorThrown) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Login Failed',
                        text: 'Something when wrong',
                        showConfirmButton: false,
                        timer: '2000'
                    })

                }
            })
        })

        $(document).on('click', '#btn-save', function(e) {

            $("#modal-edit").modal('toggle');

            var name = $("#name").val()
            var user_id = $("#user_id").val()
            var phone_number = $("#phone_number").val()
            var address = $("#address").val()
            var role_id = $("#role_id option:selected").val()
            var status = ''
            if ($('#status').is(':checked')) {
                status = 1
            } else {
                status = 0
            }

            $.ajax({
                type: 'POST',
                url: `{{ route('admin.updateUserData') }}`,
                data: {
                    user_id: user_id,
                    name: name,
                    phone_number: phone_number,
                    role_id: role_id,
                    status: status,
                    address: address,
                    _token: `{{ csrf_token() }}`
                },
                success: function(res) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Success',
                        text: res.message,
                        showConfirmButton: false,
                        timer: '2000'
                    })
                    getDataUser();
                },
                error: function(jqXHR, error, errorThrown) {

                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: 'This field should not be left blank',
                        showConfirmButton: false,
                        timer: '2000'
                    })

                }
            })

        })

        function getDataUser() {
            $('.user_datatable').DataTable().destroy();
            var table = $('.user_datatable').DataTable({
                lengthChange: false,
                processing: true,
                responsive: true,
                autoWidth: false,
                oLanguage: {
                    oPaginate: {
                        sNext: '<i>Next</i>',
                        sPrevious: '<i>Previous</i> '
                    }
                },
                iDisplayLength: 5,
                pagingType: 'simple_numbers',
                serverSide: true,
                ajax: `{{ route('admin.getUserData') }}`,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: true,
                    },
                    {
                        data: 'data.name',
                        name: 'data.name',
                        orderable: false,
                    },
                    {
                        data: 'data.email',
                        name: 'data.email',
                        orderable: false,
                    },
                    {
                        data: 'data.username',
                        name: 'data.username',
                        orderable: false,
                    },
                    {
                        data: 'data.roles.role_name',
                        name: 'data.roles.role_name',
                        orderable: false,
                    },
                    {
                        data: 'status',
                        name: 'data.status',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'action',
                        name: 'data.action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });
        }
    </script>

@endsection
