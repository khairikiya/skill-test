Keterangan Projek

- Framework Laravel 8
- ORM Elequent
- DomPdf
- Laravel migrate
- Ajax
- Jquery
- MySQL
- Bootstrap
- Box-icon
- Yajra Datatables
- Sweetalert



Note :

Running projek bisa dengan cara import database yang ada di projek secara manual
atau bisa dengan cara melakukan command 'php artisan:migrate' dengan tujuan database 'skill_test'
struktur file .env dapat diambil dalam file .env-example

ketika import atau migrate database maka akan terbentuk user default
username : admin
password : admin

username : operator
password : operator